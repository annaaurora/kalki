use clap::Parser;

#[derive(Parser, Debug)]
#[clap(name = "kalki", version, about="Do simple calcutions.")]
struct Args {
    /// If the output should be just the result or the whole equation
    #[clap(long, short = 'r')]
    only_result: bool,
    /// The first number for the calculation
    #[clap(long, short)]
    first_number: f64,
    /// The operator for the calculation
    #[clap(long, short = 'o' )]
    operator: char,
    /// The second number for the calculation
    #[clap(long, short)]
    second_number: f64,
}

fn main() {
    let args = Args::parse();
    let first_number = args.first_number;
    let operator = args.operator;
    let second_number = args.second_number;

    let result = match operator {
        '+' => first_number + second_number,
        '-' => first_number - second_number,
        '*' | 'x' => first_number * second_number,
        '/' => first_number / second_number,
        _ => panic!("Use either +, -, *, x, or / as an operator!"),
    };

    if args.only_result {
        println!("{}", result);
    } else {
        println!("{first_number} {operator} {second_number} = {result}");
    };
}

